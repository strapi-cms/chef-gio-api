let db_dev = require('./db.dev.js')
let db_prod = require('./db.prod.js')

module.exports = ({ env }) => 
env('NODE_ENV') === "development" ? 
db_dev({env})
:
db_prod({env})
